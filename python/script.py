import webiopi
import datetime

GPIO = webiopi.GPIO

M1ENABLE = 17 # GPIO pin using BCM numbering
M1FORWARD = 27
M1BACKWARD = 22
M2ENABLE = 11 # GPIO pin using BCM numbering
M2FORWARD = 9 
M2BACKWARD = 10 

HOUR_ON  = 8  # Turn Light ON at 08:00
HOUR_OFF = 18 # Turn Light OFF at 18:00

# setup function is automatically called at WebIOPi startup
def setup():
    # set the GPIO used by the light to output
    GPIO.setFunction(M1ENABLE, GPIO.OUT)
    GPIO.setFunction(M1FORWARD, GPIO.OUT)
    GPIO.setFunction(M1BACKWARD, GPIO.OUT)
    GPIO.setFunction(M2ENABLE, GPIO.OUT)
    GPIO.setFunction(M2FORWARD, GPIO.OUT)
    GPIO.setFunction(M2BACKWARD, GPIO.OUT)

    GPIO.digitalWrite(M1ENABLE, GPIO.HIGH)
    GPIO.digitalWrite(M2ENABLE, GPIO.HIGH)

# loop function is repeatedly called by WebIOPi 
def loop():
    # retrieve current datetime
    #now = datetime.datetime.now()

    # toggle light ON all days at the correct time
    #if ((now.hour == HOUR_ON) and (now.minute == 0) and (now.second == 0)):
    #    if (GPIO.digitalRead(LIGHT) == GPIO.LOW):
    #        GPIO.digitalWrite(LIGHT, GPIO.HIGH)

    # toggle light OFF
    #if ((now.hour == HOUR_OFF) and (now.minute == 0) and (now.second == 0)):
    #    if (GPIO.digitalRead(LIGHT) == GPIO.HIGH):
    #        GPIO.digitalWrite(LIGHT, GPIO.LOW)

    # gives CPU some time before looping again
    webiopi.sleep(1)

# destroy function is called at WebIOPi shutdown
def destroy():
    GPIO.digitalWrite(M1ENABLE, GPIO.LOW)
    GPIO.digitalWrite(M1FORWARD, GPIO.LOW)
    GPIO.digitalWrite(M1BACKWARD, GPIO.LOW)
    GPIO.digitalWrite(M2ENABLE, GPIO.LOW)
    GPIO.digitalWrite(M2FORWARD, GPIO.LOW)
    GPIO.digitalWrite(M2BACKWARD, GPIO.LOW)

@webiopi.macro
def Left():
    GPIO.digitalWrite(M1ENABLE, GPIO.HIGH)
    GPIO.digitalWrite(M1FORWARD, GPIO.HIGH)
    GPIO.digitalWrite(M1BACKWARD, GPIO.LOW)
    GPIO.digitalWrite(M2ENABLE, GPIO.HIGH)
    GPIO.digitalWrite(M2FORWARD, GPIO.LOW)
    GPIO.digitalWrite(M2BACKWARD, GPIO.HIGH)

@webiopi.macro
def Right():
    GPIO.digitalWrite(M1ENABLE, GPIO.HIGH)
    GPIO.digitalWrite(M1FORWARD, GPIO.LOW)
    GPIO.digitalWrite(M1BACKWARD, GPIO.HIGH)
    GPIO.digitalWrite(M2ENABLE, GPIO.HIGH)
    GPIO.digitalWrite(M2FORWARD, GPIO.HIGH)
    GPIO.digitalWrite(M2BACKWARD, GPIO.LOW)

@webiopi.macro
def Forward():
    GPIO.digitalWrite(M1ENABLE, GPIO.HIGH)
    GPIO.digitalWrite(M1FORWARD, GPIO.LOW)
    GPIO.digitalWrite(M1BACKWARD, GPIO.HIGH)
    GPIO.digitalWrite(M2ENABLE, GPIO.HIGH)
    GPIO.digitalWrite(M2FORWARD, GPIO.LOW)
    GPIO.digitalWrite(M2BACKWARD, GPIO.HIGH)

@webiopi.macro
def Backward():
    GPIO.digitalWrite(M1ENABLE, GPIO.HIGH)
    GPIO.digitalWrite(M1FORWARD, GPIO.HIGH)
    GPIO.digitalWrite(M1BACKWARD, GPIO.LOW)
    GPIO.digitalWrite(M2ENABLE, GPIO.HIGH)
    GPIO.digitalWrite(M2FORWARD, GPIO.HIGH)
    GPIO.digitalWrite(M2BACKWARD, GPIO.LOW)

@webiopi.macro
def Stop():
    GPIO.digitalWrite(M1ENABLE, GPIO.LOW)
    GPIO.digitalWrite(M1FORWARD, GPIO.LOW)
    GPIO.digitalWrite(M1BACKWARD, GPIO.LOW)
    GPIO.digitalWrite(M2ENABLE, GPIO.LOW)
    GPIO.digitalWrite(M2FORWARD, GPIO.LOW)
    GPIO.digitalWrite(M2BACKWARD, GPIO.LOW)

